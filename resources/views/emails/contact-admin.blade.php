
Üzenet a  {{ env('APP_NAME') }} oldalon keresztül.

<p>
Időpont: {{ date('Y-m-d H:i:s') }}<br />
Küldő email:
{{ $maildata->email }}<br />
Név:
{{ $maildata->name }}
</p>
<p>
    {{ $maildata->msg }}
</p>
