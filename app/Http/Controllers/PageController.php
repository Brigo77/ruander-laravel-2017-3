<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use Validator;
use Mail;
use Session;

class PageController extends Controller
{
    public function about() {
        $firstname = 'Brigi';
        $lastname = 'Unger';
//        return view('pages.about')->with('firstname',$firstname)->with('lastname', $lastname);
//        return view('pages.about', ['firstname'=>$firstname, 'lastname'=>$lastname]);
        return view('pages.about', compact('firstname', 'lastname'))->with('time', time()); //ezek a lehetőségek vannak, álltalában az utolsót használjuk
    }
    
    public function contact() {
//        Log::info('valami');
        return view('pages.contact');
    }
    
    public function send(Request $request) {
        
        $rules = [
            'email' => 'required|email',
            'name' => 'required',
            'msg' => 'required|min:10',
        ];
        
//        dd(validator($request->all(), $rules));
        
        $this->validate($request, $rules);
        // küldhetjük az üzenetet
        
        Mail::send('emails.contact', ['maildata' => $request], function ($m) use ($request) { //user emlékeztető email, nekem és a usernek.
            $m->from(env('MAIL_FROM_ADDRESS'), env('APP_NAME'));
            $m->to($request->email, $request->name )->subject('Contact form message');
        });
         
        Mail::send('emails.contact-admin', ['maildata' => $request], function ($m) use ($request) { //oldal üzemeltető
            $m->from(env('MAIL_FROM_ADDRESS'), $request->name );
            $m->replyTo($request->email, $request->name );
            $m->to('support@mydomain.hu', env('APP_NAME') )->subject('A user set a contact message');
        });
        
        Session::flash('status', 'Message sent successfully!');
//        dd($request->session()->pull('status'));
        return redirect()->back(); // vissza az előző oldalra
        
//        dd('volt egy send'); //dieDump kiir és megáll, lehet dumo-olni is
//        dd($request->all());
//        dd($request->name); //igy lehet csak egy mező adatát visszakérni
//        dd($request->msg);
//        dd($request->email);
        
    }
}
